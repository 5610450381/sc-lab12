import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	public static void main(String[] args) {

		try {
			FileReader fileReader = new FileReader("phonebook.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			System.out.println("--------- Java Phone Book ---------"+"\n"+"Name Phone"+"\n"+"==== =====");
			String line = buffer.readLine();
			while (line != null) {
				System.out.println(line);
				line = buffer.readLine();
			}		 
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
	}
}

